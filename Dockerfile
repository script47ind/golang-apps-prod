FROM golang:latest
WORKDIR /APP
ADD . .
RUN go mod download
RUN go build -race -ldflags "-extldflags '-static'" -o ./tanuki
RUN echo "#!/bin/bash \n ./tanuki \n sleep 24h" > start.sh
RUN chmod +x ./start.sh
CMD ["./start.sh"]
